﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mantenimiento_tribunaAPI.Models
{
    public class RespuestaIncidencia
    {
        public Guid IdRef { get; set; }
         public string Categoria { get; set; }
        public string Solucion { get; set; }
        public double Tiempo_Invertido { get; set; } //En Horas
        public Guid Responsable { get; set; }
    }
}
