﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Mantenimiento_tribunaAPI.Models
{
    public class LoginModel
    {
        [Required(ErrorMessage = "Nombre de usuario requerido")]
        public string NombreUsuario { get; set; }
        [Required(ErrorMessage = "Contraseña requerida")]
        public string Contraseña { get; set; }
    }
}
