﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Mantenimiento_tribunaAPI.Models
{
    public class Empleados
    {
        [Key]
        public Guid ID { get; set; }

        [Column(TypeName = "varchar(50)")]
        [Required(ErrorMessage = "Nombre completo requerido")]
        public string Nombre { get; set; }

        [Column(TypeName = "varchar(50)")]
        [Required(ErrorMessage = "Puesto laboral requerido")]
        public string Puesto { get; set; }

        [Column(TypeName = "varchar(50)")]
        [Required(ErrorMessage = "Area requerida")]
        public string Area { get; set; }

        [Column(TypeName = "varchar(50)")]
        [Required(ErrorMessage = "Plaza requerida")]
        public string Plaza { get; set; }

        [Column(TypeName = "varchar(50)")]
        [Required(ErrorMessage = "Telefono de contacto requerido")]
        [MinLength(10, ErrorMessage = "Formato de telefono incorrecto")]
        [MaxLength(10, ErrorMessage = "Formato de telefono incorrecto")]
        public string Telefono { get; set; }

        [Column(TypeName = "datetime")]
        public DateTime Fecha_Registro { get; set; }

    }
}
