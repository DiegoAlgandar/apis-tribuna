﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Mantenimiento_tribunaAPI.Models
{
    public class Equipo_Inventario
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [DefaultValue("newsequentialid()")]
        public Guid ID { get; set; } 
        [Column(TypeName = "datetime")]
        [DefaultValue("getdate()")]
        public DateTime Fecha_Registro { get; set; }
        [Column(TypeName = "varchar(50)")]
        [Required(ErrorMessage = "Nombre requerido")]
        public string Nombre_Equipo { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string? Marca { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string? Tipo { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string? Modelo { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string? N_Serie { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string? Procesador { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string? SO { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string? Office { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string? Estado { get; set; }
        public Guid? Usuario_Actual { get; set; }
        public int? Disco_Duro { get; set; }
        public string? N_Telefono { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string? Plan { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string? Razon_Social { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string? Region { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string? Cuenta { get; set; }
        [Column(TypeName = "varchar(50)")]
        public string? Cuenta_Padre { get; set; }
    }
}
