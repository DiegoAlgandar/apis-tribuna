﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Mantenimiento_tribunaAPI.Models
{
    public class Usuario
    {
        [Key]
        public Guid ID { get; set; }
        [Required(ErrorMessage = "Campo requerido")]
        public string NombreUsuario { get; set; }
        [Required(ErrorMessage = "Campo requerido")]
        public string Nombre { get; set; }
        [Required(ErrorMessage = "Campo requerido")]
        public string Apellido_Paterno { get; set; }
        [Required(ErrorMessage = "Campo requerido")]
        public string Apellido_Materno { get; set; }
        
        [Required(ErrorMessage = "Campo requerido")]
        [MinLength(8, ErrorMessage = "La contraseña debe tener al menos 8 caracteres")]
        public string Contraseña { get; set; }

        public DateTime Fecha_Registro { get; set; }

        public string Rol { get; set; }
        public bool Autorizado { get; set; }

        [Required(ErrorMessage = "Campo requerido")]
        public Guid EmpleadoID { get; set; }
    }
}
