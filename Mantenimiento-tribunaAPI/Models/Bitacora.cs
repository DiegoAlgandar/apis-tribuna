﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace Mantenimiento_tribunaAPI.Models
{
    public class Bitacora
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid ID { get; set; }
        [Column(TypeName = "datetime")]//tipo exacto de sql
        public DateTime Fecha_Registro { get; set; }
        public Guid UsuarioID { get; set; }
        [Required]
        public string Estado { get; set; } //Inicial => Abierto, Modificable a Cerrado
        [Required]
        public string Area { get; set; } // AreaID para relacionar con otra tabla
        [Required]
        public string Plaza { get; set; }
        [Required]
        public string Tipo { get; set; }
        [Required]
        public string Problema { get; set; }
        [Required]
        public string Prioridad { get; set; }

        //Datos que se actualizan 
        public string Categoria { get; set; }
        public string Solucion { get; set; }
        public double Tiempo_Invertido { get; set; } //En Horas
        public string Responsable { get; set; }
        [Column(TypeName = "datetime")]//tipo exacto de sql

        public DateTime Fecha_Cierre { get; set; }

    }
}
