﻿using Mantenimiento_tribunaAPI.Models;
using Mantenimiento_tribunaAPI.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mantenimiento_tribunaAPI.Services
{
    public class UsuariosService
    {
        private UsuariosPersistence _usuariosPersistence;

        public UsuariosService(UsuariosPersistence usuariosPersistence) => _usuariosPersistence = usuariosPersistence;
        public string nuevoUsuario(Usuario usuario) => _usuariosPersistence.nuevoUsuario(usuario);
        public Usuario iniciarSesion(string user, string password) => _usuariosPersistence.iniciarSesion(user, password);
    }
}
