﻿using Mantenimiento_tribunaAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mantenimiento_tribunaAPI.Services.Interfaces
{
    public interface IAuthService
    {
        object CreateToken(Usuario user);
    }
}
