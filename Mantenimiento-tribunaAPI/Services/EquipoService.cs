﻿using Mantenimiento_tribunaAPI.Models;
using Mantenimiento_tribunaAPI.Persistence;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mantenimiento_tribunaAPI.Services
{
    public class EquipoService
    {
        private EquipoPersistence _equipoPersistence;

        public EquipoService(EquipoPersistence equipoPersistence) => _equipoPersistence = equipoPersistence;
        public List<Equipo_Inventario> equiposLibres(string tipo)=> _equipoPersistence.equiposLibres(tipo);
        public object asignarEquipo(Asignacion model) => _equipoPersistence.asignarEquipo(model);
        public string devolverEquipo(Guid equipoID) => _equipoPersistence.devolverEquipo(equipoID);
        public Equipo_Inventario equipoInfo(Guid ID) => _equipoPersistence.equipoInfo(ID);

        public List<Equipo_Inventario> misAsignados(Guid empleadoID) => _equipoPersistence.misAsignados(empleadoID);

        public List<Equipo_Inventario> obtenerEquipos() => _equipoPersistence.obtenerEquipos();
        public Equipo_Inventario registrarEquipo(Equipo_Inventario model) => _equipoPersistence.registrarEquipo(model);
        public Equipo_Inventario actualizarEquipo(Equipo_Inventario model, Guid ID) => _equipoPersistence.actualizarEquipo(model,ID);
    }
}
