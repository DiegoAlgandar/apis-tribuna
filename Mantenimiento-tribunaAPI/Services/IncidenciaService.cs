﻿using Mantenimiento_tribunaAPI.Models;
using Mantenimiento_tribunaAPI.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mantenimiento_tribunaAPI.Services
{
    public class IncidenciaService
    {
        private IncidenciaPersistence _incidenciaPersistence;

        public IncidenciaService(IncidenciaPersistence incidenciaPersistence)
        {
            _incidenciaPersistence = incidenciaPersistence;
        }

        public string nuevaIncidencia(Bitacora bit) => _incidenciaPersistence.nuevaIncidencia(bit);
        public Bitacora resolverIncidencia(RespuestaIncidencia bit) => _incidenciaPersistence.resolverIncidencia(bit);
        public IEnumerable<Bitacora> obtenerIncidencias() => _incidenciaPersistence.obtenerIncidencias();
        public IEnumerable<Bitacora> incidencias_Resueltas() => _incidenciaPersistence.incidencias_Resueltas();
    }
}
