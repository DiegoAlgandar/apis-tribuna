﻿using Mantenimiento_tribunaAPI.Models;
using Mantenimiento_tribunaAPI.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mantenimiento_tribunaAPI.Services
{
    public class EmpleadoService
    {
        private EmpleadosPersistence _empleadosPersistence;
        public EmpleadoService(EmpleadosPersistence empleadosPersistence) => _empleadosPersistence = empleadosPersistence;

        public IEnumerable<Empleados> obtenerEntradas(int startPosition)
        {
            var res = _empleadosPersistence.obtenerEntradas(startPosition);
            return res;
        }

        public string nuevoEmpleado(Empleados model) => _empleadosPersistence.nuevoEmpleado(model);

        public string eliminar(Guid ID) => _empleadosPersistence.eliminar(ID);

        internal IEnumerable<Empleados> buscarPorNombre(string nombre)
        {
            var res = _empleadosPersistence.buscarPorNombre(nombre);
            return res;
        }
    }
}
