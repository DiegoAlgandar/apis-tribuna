﻿using Mantenimiento_tribunaAPI.Models;
using Mantenimiento_tribunaAPI.Services.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Mantenimiento_tribunaAPI.Services
{
    public class AuthService : IAuthService
    {
        private readonly SymmetricSecurityKey _sskey;

        public AuthService(IConfiguration config)
        {
            _sskey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(config["Token"]));
        }

        public object CreateToken(Usuario user)
        {
            var claims = new List<Claim>{
                new Claim("ID", user.ID.ToString()),
                new Claim(JwtRegisteredClaimNames.Name, user.NombreUsuario),
                new Claim("Rol", user.Rol)

            };

            var credentials = new SigningCredentials(_sskey, SecurityAlgorithms.HmacSha512Signature);

            var tokenDescription = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.UtcNow.AddHours(3),
                SigningCredentials = credentials,
                
            };

            var tokenHandler = new JwtSecurityTokenHandler();

            var token = tokenHandler.CreateToken(tokenDescription);
            var tokenwritten = tokenHandler.WriteToken(token);
            var obj = new { 
                tokenDescription.Expires, 
                token=tokenwritten,
                usuario = user.NombreUsuario,
                user.Rol,
                user.ID,
                user.EmpleadoID
            };

            return obj;
        }
        
    }
}
