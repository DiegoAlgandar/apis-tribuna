﻿using Mantenimiento_tribunaAPI.Context;
using Mantenimiento_tribunaAPI.Models;
using Mantenimiento_tribunaAPI.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mantenimiento_tribunaAPI.Controllers
{
    //[Authorize]
    [Route("api/[controller]")]
    public class EmpleadosController : Controller
    {
        private EmpleadoService _empleadoService;
        public AppDBContext _DB;
        public EmpleadosController(EmpleadoService empleadoService, AppDBContext DB)
        {
            _empleadoService = empleadoService;
            _DB = DB;
        }
        [HttpGet("Todos")]
        public ActionResult<IEnumerable<Empleados>> Todos()
        {
            //query para obtener 10 resultados a partir de una posicion, por default startPosition es 0
            var res = _DB.Empleados.ToList();
            return Ok(res);
        }

        [HttpGet("Paginacion/{startPosition}")]
        public ActionResult<IEnumerable<Empleados>> Paginacion(int startPosition)
        {
            var res = _empleadoService.obtenerEntradas(startPosition);
            return Ok(res);
        }
        [HttpGet("Consulta/{ID}")]
        public ActionResult<object> consulta(Guid ID)
        {
            var res = _DB.Empleados.FirstOrDefault(x => x.ID.Equals(ID));
            return Ok(res);
        }
        [HttpGet("{nombre}")]
        public IEnumerable<Empleados> Paginacion(string nombre)
        {
            return _empleadoService.buscarPorNombre(nombre);
        }
        [HttpPost]
        public ActionResult<string> nuevo_Empleado([FromBody] Empleados model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var res = _empleadoService.nuevoEmpleado(model);
            if (res == "1")
                return Ok("Empleado registrado con exito");
            else
                return BadRequest(new
                {
                    message = "Ocurrio un error al registar"
                });
        }
        [HttpDelete("{ID}")]
        public string eliminar(Guid ID)
        {
            var res = _empleadoService.eliminar(ID);
            return res == "1" ? "Eliminado con exito" : res;

        }

    }
}
