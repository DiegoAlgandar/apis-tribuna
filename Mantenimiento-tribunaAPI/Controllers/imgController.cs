﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Mantenimiento_tribunaAPI.Context;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;

namespace Mantenimiento_tribunaAPI.Controllers
{
    [Route("api/[controller]")]

    public class imgController : Controller
    {
        private readonly AppDBContext _db;
        private IWebHostEnvironment _env;
        public imgController(AppDBContext db, IWebHostEnvironment env)
        {
            _db = db;
            _env = env;
        }

        [HttpGet]
        public IEnumerable<Imgs> get()
        {
            return _db.Imgs.ToList();
        }
        [HttpPost]
        public async Task<IActionResult> Upload(string ID)
        {
            string folder = "uploads";
            string fullpath = Path.Combine(_env.ContentRootPath, folder);
            bool exists = System.IO.Directory.Exists(fullpath);

            if (!exists)
                Directory.CreateDirectory(folder);

            foreach (var file in Request.Form.Files)
            {
                if (file.Length > 0)
                {
                    string filePath = Path.Combine(fullpath, file.FileName);
                    using (Stream fileStream = new FileStream(filePath, FileMode.Create))
                    {
                        await file.CopyToAsync(fileStream);
                        fileStream.Close();
                        fileStream.Dispose();
                    }
                    var shortPath = folder + "/" + file.FileName;

                    Imgs img = new Imgs()
                    {
                        Ruta = shortPath,
                        EquipoID = Guid.Parse(ID)
                    };

                    await _db.Imgs.AddAsync(img);
                    _db.SaveChanges();
                }
            }
            return Ok("Imagen guardada con exito");
        }
        [HttpGet("{name}")]
        public ActionResult Get(string name)
        {
            string folder = "uploads";
            var filePath = Path.Combine(_env.ContentRootPath, folder);
            var files = Directory.GetFiles(filePath);
            string path = "";
            bool isFound = false;

            foreach (var currentfile in files)
            {
                //C:\Users\eldie\Desktop\apis-tribuna\Mantenimiento-tribunaAPI\uploads\ssss.PNG
                isFound = currentfile.Contains(name);
                if (isFound)
                {
                    var extension = currentfile.Split(".")[1];
                    var file = PhysicalFile(filePath, $"image/{extension}", $"{name}.{extension}");
                    path = "uploads/" + file.FileDownloadName;
                    //path = file.FileName+$@"\{file.FileDownloadName}";
                    return Ok(path);
                }

            }
            if (!isFound)
            {
                StatusCode(404);
                return BadRequest("Imagen no encontrada");
            }
            return Ok(path);
        }
        [HttpGet("Consulta/{ID}")]
        public ActionResult<IEnumerable<Imgs>> getImagesByEquiID(Guid ID)
        {
            var res = _db.Imgs.Where(x => x.EquipoID.Equals(ID)).ToList();
            if(res == null)
            {
                return BadRequest("ocurrio un error");
            }
            return Ok(res);
        }
        [HttpDelete("{ID}")]
        public ActionResult borrarImagenes(Guid ID)
        {
            string folder = "uploads";
            string fullpath = Path.Combine(_env.ContentRootPath, folder);
            var files = Directory.GetFiles(fullpath);

            var res = _db.Imgs.Where(x=>x.EquipoID.Equals(ID)).ToList();
            foreach (var item in res)
            {
                foreach (var file in files)
                {
                    var fileRef = item.Ruta.Split("/")[1];
                    var name = file.Split("uploads\\")[1];
                    var isFound = name.Equals(fileRef);
                    if (isFound)
                    {
                        Console.WriteLine("Borrar");
                        System.IO.File.Delete(file);
                    }
                }
                _db.Imgs.Attach(item);
                _db.Imgs.Remove(item);
                _db.SaveChanges();
            }
            return Ok("Borradas con exito");
        }
    }
    public class Imgs
    {
        public int ID { get; set; }
        public string Ruta { get; set; }
        public Guid EquipoID { get; set; }
    }
}
