﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mantenimiento_tribunaAPI.Context;
using Mantenimiento_tribunaAPI.Models;
using Mantenimiento_tribunaAPI.Services;
using Mantenimiento_tribunaAPI.Services.Interfaces;
using Microsoft.AspNetCore.Mvc;

namespace Mantenimiento_tribunaAPI.Controllers
{
    [Route("api/[controller]")]
    public class UsuariosController : Controller
    {
        private UsuariosService _usuariosService;
        private readonly IAuthService _authService;
        private readonly AppDBContext _db;
        public UsuariosController(UsuariosService usuariosService, IAuthService authService, AppDBContext db)
        {
            _usuariosService = usuariosService;
            _authService = authService;
            _db = db;
        }
        [HttpGet]
        public ActionResult<string> serverStart()
        {
            return "El Servidor Esta Corriendo, Puede Probar Sus Controladores :)";
        }

        [HttpGet("Confirmados")]
        public ActionResult<IEnumerable<Usuario>> obtenerTodos()
        {
            var res = _db.Usuarios.Where(x => x.Autorizado.Equals(true)).ToList();
            return Ok(res);
        }
        [HttpGet("Registros_Pendientes")]
        public ActionResult<IEnumerable<Usuario>> RegistrosPendientes()
        {
            var res = _db.Usuarios.Where(x=>x.Autorizado == false).ToList();
            return Ok(res);
        }
        [HttpGet("Consulta/{ID}")]
        public ActionResult<Usuario> consulta(Guid ID)
        {
            var res = _db.Usuarios.FirstOrDefault(x => x.ID.Equals(ID));
            return Ok(res);
        }

        [HttpPost("Nuevo_Usuario")]
        public ActionResult NuevoUsuario([FromBody] Usuario usuario)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                var result = _usuariosService.nuevoUsuario(usuario);
                if (result == "-1") return StatusCode(500);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpPost("IniciarSesion")]
        public ActionResult<Usuario> IniciarSesion([FromBody] LoginModel loginModel)
        {
            try
            {
                if (!ModelState.IsValid)
                    return BadRequest(ModelState);

                var res = _usuariosService.iniciarSesion(loginModel.NombreUsuario, loginModel.Contraseña);

                var sesion = _authService.CreateToken(res);

                return Ok(sesion);
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex.Message);

            }


        }
        [HttpPut("ConfirmarRegistro")]
        public ActionResult<string> ConfirmarRegistro([FromBody] Guid ID)
        {
            var res = _db.Usuarios.FirstOrDefault(x=>x.ID.Equals(ID));

            if (res != null)
            {
                res.Autorizado = true;
                _db.SaveChanges();
            }
            else return BadRequest("No encontrado");

            return Ok("Registro Autorizado");
        }

        [HttpDelete("EliminarUsuario/{ID}")]
        public ActionResult<string> eliminarUsuario(Guid ID)
        {
            var res = _db.Usuarios.FirstOrDefault(x => x.ID.Equals(ID));

            if (res != null)
            {
                _db.Usuarios.Remove(res);
                _db.SaveChanges();
            }
            else return BadRequest("No encontrado");

            return Ok("Usuario Eliminado");
        }
    }
}
