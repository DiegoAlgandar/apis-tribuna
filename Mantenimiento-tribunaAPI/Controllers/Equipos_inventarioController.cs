﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Mantenimiento_tribunaAPI.Context;
using Mantenimiento_tribunaAPI.Models;
using Mantenimiento_tribunaAPI.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Mantenimiento_tribunaAPI.Controllers
{
    [Route("api/[controller]")]

    public class Equipos_inventarioController : Controller
    {
        public EquipoService _equipoService;
        public Equipos_inventarioController(EquipoService equipoService)
        {
            _equipoService = equipoService;
        }
        [HttpGet("Asignados/{empleadoID}")]
        public ActionResult<IEnumerable<Equipo_Inventario>> misAsignados(Guid empleadoID)
        {
            var res =  _equipoService.misAsignados(empleadoID);
            if (res == null)
                return BadRequest("No tienes equipos asignados");
            return res;
        }
        [HttpGet("{ID}")]
        public ActionResult<Equipo_Inventario> obtenerEquipo(Guid ID)
        {
            var res = _equipoService.equipoInfo(ID);
            if (res == null)
                return BadRequest("Equipo no encontrado");
            return res;
        }
        [HttpGet]
        public ActionResult<IEnumerable<Equipo_Inventario>> obtenerEquipos()
        {
            return _equipoService.obtenerEquipos();
        }
        [HttpPost]
        public ActionResult<Equipo_Inventario> nuevoEquipo([FromBody] Equipo_Inventario model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var res = _equipoService.registrarEquipo(model);
            return Ok(model);  
        }
        [HttpPut("{ID}")]
        public ActionResult<Equipo_Inventario> actualizarEquipo(Guid ID, [FromBody] Equipo_Inventario model)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            var res = _equipoService.actualizarEquipo(model,ID);
            return Ok(model);
        }
        [HttpGet("{tipo}/Libres")]
        public ActionResult<IEnumerable<Equipo_Inventario>> equiposLibres(string tipo)
        {
            var res = _equipoService.equiposLibres(tipo);
            if (res.Count() == 0)
                return BadRequest("No se encontro resultados del tipo solicitado");
            return Ok(res);
        }
        [HttpPut("Asignar_Equipo")]
        public ActionResult<object> Asignar([FromBody] Asignacion model)
        {
            var res = _equipoService.asignarEquipo(model);
            if (res == null)
                return BadRequest("No se encontró un equipo para asignar");
            return Ok(res);
        }
        [HttpPut("Devolver_equipo/{equipoID}")]
        public ActionResult<string> devolverEquipo(Guid equipoID)
        {
            var res = _equipoService.devolverEquipo(equipoID);
            if (res == null)
            {
                return BadRequest("No se encontró el equipo a devolver");
            }
            return Ok("Equipo devuelto con éxito");
        }
    }
}
