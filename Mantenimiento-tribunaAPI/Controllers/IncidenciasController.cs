﻿using Mantenimiento_tribunaAPI.Context;
using Mantenimiento_tribunaAPI.Models;
using Mantenimiento_tribunaAPI.Persistence;
using Mantenimiento_tribunaAPI.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mantenimiento_tribunaAPI.Controllers
{
    [Route("api/[controller]")]
    public class IncidenciasController : Controller
    {
        private IncidenciaService _incidenciaService;
        private AppDBContext _db;
        public IncidenciasController(IncidenciaService incidenciaService, AppDBContext db)
        {
            _incidenciaService = incidenciaService;
            _db = db;
        }

        [HttpGet]
        public ActionResult<IEnumerable<Bitacora>> Get()
        {
            var res = _incidenciaService.obtenerIncidencias();
            return Ok(res);
        }
        [HttpGet("Resueltas")]
        public ActionResult<IEnumerable<Bitacora>> todas_Incidencias()
        {
            var res = _incidenciaService.incidencias_Resueltas();
            return Ok(res);
        }
        [HttpPost]
        public ActionResult<string> nuevaIncidencia([FromBody] Bitacora bit)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            try
            {
                var result = _incidenciaService.nuevaIncidencia(bit);
                if (result == "-1") return StatusCode(500);
                return Ok(result);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
        [HttpPut("Resolver")]
        public ActionResult<Bitacora> Resolver([FromBody] RespuestaIncidencia respuestaIncidencia)
        {
            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            try
            {
                var res = _incidenciaService.resolverIncidencia(respuestaIncidencia);
                if(res == null)
                    return BadRequest();

                res.Fecha_Cierre = DateTime.Now;
                res.Estado = "Cerrado";
                res.Responsable = respuestaIncidencia.Responsable.ToString();
                res.Solucion = respuestaIncidencia.Solucion;
                res.Tiempo_Invertido = respuestaIncidencia.Tiempo_Invertido;
                res.Categoria = respuestaIncidencia.Categoria;

                _db.Incidencias.Update(res);
                _db.SaveChanges();
                return Ok(res);
            }
            catch (Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
