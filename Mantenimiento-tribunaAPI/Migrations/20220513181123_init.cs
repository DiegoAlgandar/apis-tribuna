﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Mantenimiento_tribunaAPI.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Empleados",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    Nombre = table.Column<string>(type: "varchar(50)", nullable: false),
                    Puesto = table.Column<string>(type: "varchar(50)", nullable: false),
                    Area = table.Column<string>(type: "varchar(50)", nullable: false),
                    Plaza = table.Column<string>(type: "varchar(50)", nullable: false),
                    Telefono = table.Column<string>(type: "varchar(50)", maxLength: 10, nullable: false),
                    Fecha_Registro = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "getdate()")
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Empleados", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Equipos",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    Fecha_Registro = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "getdate()"),
                    Nombre_Equipo = table.Column<string>(type: "varchar(50)", nullable: false),
                    Marca = table.Column<string>(type: "varchar(50)", nullable: true),
                    Tipo = table.Column<string>(type: "varchar(50)", nullable: true),
                    Modelo = table.Column<string>(type: "varchar(50)", nullable: true),
                    N_Serie = table.Column<string>(type: "varchar(50)", nullable: true),
                    Procesador = table.Column<string>(type: "varchar(50)", nullable: true),
                    SO = table.Column<string>(type: "varchar(50)", nullable: true),
                    Office = table.Column<string>(type: "varchar(50)", nullable: true),
                    Estado = table.Column<string>(type: "varchar(50)", nullable: true),
                    Usuario_Actual = table.Column<Guid>(nullable: true),
                    Disco_Duro = table.Column<int>(nullable: true),
                    N_Telefono = table.Column<string>(nullable: true),
                    Plan = table.Column<string>(type: "varchar(50)", nullable: true),
                    Razon_Social = table.Column<string>(type: "varchar(50)", nullable: true),
                    Region = table.Column<string>(type: "varchar(50)", nullable: true),
                    Cuenta = table.Column<string>(type: "varchar(50)", nullable: true),
                    Cuenta_Padre = table.Column<string>(type: "varchar(50)", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Equipos", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Imgs",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Ruta = table.Column<string>(nullable: true),
                    EquipoID = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Imgs", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Incidencias",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    Fecha_Registro = table.Column<DateTime>(type: "datetime", nullable: false, defaultValueSql: "getdate()"),
                    UsuarioID = table.Column<Guid>(nullable: false),
                    Estado = table.Column<string>(nullable: false),
                    Area = table.Column<string>(nullable: false),
                    Plaza = table.Column<string>(nullable: false),
                    Tipo = table.Column<string>(nullable: false),
                    Problema = table.Column<string>(nullable: false),
                    Prioridad = table.Column<string>(nullable: false),
                    Categoria = table.Column<string>(nullable: true),
                    Solucion = table.Column<string>(nullable: true),
                    Tiempo_Invertido = table.Column<double>(nullable: true),
                    Responsable = table.Column<string>(nullable: true),
                    Fecha_Cierre = table.Column<DateTime>(type: "datetime", nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Incidencias", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "Usuarios",
                columns: table => new
                {
                    ID = table.Column<Guid>(nullable: false, defaultValueSql: "newsequentialid()"),
                    NombreUsuario = table.Column<string>(nullable: false),
                    Nombre = table.Column<string>(nullable: false),
                    Apellido_Paterno = table.Column<string>(nullable: false),
                    Apellido_Materno = table.Column<string>(nullable: false),
                    Contraseña = table.Column<string>(nullable: false),
                    Fecha_Registro = table.Column<DateTime>(nullable: false, defaultValueSql: "getdate()"),
                    Rol = table.Column<string>(nullable: false),
                    Autorizado = table.Column<bool>(nullable: false),
                    EmpleadoID = table.Column<Guid>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Usuarios", x => x.ID);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Empleados");

            migrationBuilder.DropTable(
                name: "Equipos");

            migrationBuilder.DropTable(
                name: "Imgs");

            migrationBuilder.DropTable(
                name: "Incidencias");

            migrationBuilder.DropTable(
                name: "Usuarios");
        }
    }
}
