﻿using Mantenimiento_tribunaAPI.Controllers;
using Mantenimiento_tribunaAPI.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mantenimiento_tribunaAPI.Context
{
    public class AppDBContext: DbContext
    {
        public AppDBContext(DbContextOptions<AppDBContext> options): base(options)
        {

        }
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Empleados> Empleados { get; set; }
        public DbSet<Imgs> Imgs { get; set; }
        public DbSet<Bitacora> Incidencias { get; set; }
        public DbSet<Equipo_Inventario> Equipos { get; set; }
    }
}
