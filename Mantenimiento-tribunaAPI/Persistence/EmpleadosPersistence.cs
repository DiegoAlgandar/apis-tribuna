﻿using Mantenimiento_tribunaAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mantenimiento_tribunaAPI.Persistence
{
    public class EmpleadosPersistence
    {
        private Connections _connections;
        public EmpleadosPersistence(Connections connections) => _connections = connections;
        public IEnumerable<Empleados> obtenerEntradas(int startPosition)
        {

            string pSentence = @$"select * from Empleados order by ID asc offset {startPosition} rows fetch next 15 rows only";


            try
            {
                return _connections.queryBlogListNoAsync(pSentence, new Dictionary<string, object> { { "", "" } }, reader =>
                {

                    return new Empleados()
                    {
                        ID = reader["ID"] is DBNull ? Guid.Empty : Guid.Parse(reader["ID"].ToString()),
                        Nombre = reader["Nombre"] is DBNull ? null : reader["Nombre"].ToString(),
                        Plaza = reader["Plaza"] is DBNull ? null : reader["Plaza"].ToString(),
                        Puesto = reader["Puesto"] is DBNull ? null : reader["Puesto"].ToString(),
                        Area = reader["Area"] is DBNull ? null : reader["Area"].ToString(),
                        Telefono = reader["Telefono"] is DBNull ? null : reader["Telefono"].ToString(),
                        Fecha_Registro = reader["Fecha_Registro"] is DBNull ? DateTime.Now : DateTime.Parse(reader["Fecha_Registro"].ToString())

                    };
                });
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public string eliminar(object ID)
        {
            string pResult = "";
            string pSetencia = $"Delete from Empleados where ID = '{ID}'";

            try
            {
                _connections.executeBlog(pSetencia, (com) =>
                {
                    pResult = com.ExecuteNonQuery().ToString();
                });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                pResult = ex.Message;
            }

            return pResult;
        }
        public string nuevoEmpleado(Empleados emp)
        {
            string pResult = "";
            string pSetencia =
               "INSERT INTO Empleados " +
               "(Nombre, Puesto, Area, Plaza, Telefono)" +
               " values (@Nombre,@Puesto,@Area,@Plaza,@Telefono)";

            try
            {
                _connections.executeBlog(pSetencia, (com) =>
                {
                    com.Parameters.AddWithValue("@Nombre", emp.Nombre);
                    com.Parameters.AddWithValue("@Puesto", emp.Puesto);
                    com.Parameters.AddWithValue("@Area", emp.Area);
                    com.Parameters.AddWithValue("@Plaza", emp.Plaza);
                    com.Parameters.AddWithValue("@Telefono", emp.Telefono);
                    pResult = com.ExecuteNonQuery().ToString();
                });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                pResult = ex.Message;
            }

            return pResult;
        }
        public IEnumerable<Empleados> buscarPorNombre(string nombre)
        {
            string pSentence = @$"select * from Empleados where nombre like '%{nombre}%'";
            try
            {
                return _connections.queryBlogListNoAsync(pSentence, new Dictionary<string, object> { { "", "" } }, reader =>
                {

                    return new Empleados()
                    {
                        ID = reader["ID"] is DBNull ? Guid.Empty : Guid.Parse(reader["ID"].ToString()),
                        Nombre = reader["Nombre"] is DBNull ? null : reader["Nombre"].ToString(),
                        Plaza = reader["Plaza"] is DBNull ? null : reader["Plaza"].ToString(),
                        Puesto = reader["Puesto"] is DBNull ? null : reader["Puesto"].ToString(),
                        Area = reader["Area"] is DBNull ? null : reader["Area"].ToString(),
                        Telefono = reader["Telefono"] is DBNull ? null : reader["Telefono"].ToString(),
                        Fecha_Registro = reader["Fecha_Registro"] is DBNull ? DateTime.Now : DateTime.Parse(reader["Fecha_Registro"].ToString())

                    };
                });
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
