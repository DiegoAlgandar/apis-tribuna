﻿using Mantenimiento_tribunaAPI.Context;
using Mantenimiento_tribunaAPI.Models;
using Mantenimiento_tribunaAPI.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BC = BCrypt.Net.BCrypt;

namespace Mantenimiento_tribunaAPI.Persistence
{
    public class UsuariosPersistence
    {
        private Connections _connections;
        private readonly IAuthService _authService;
        private readonly AppDBContext _db;

        public UsuariosPersistence(Connections connections, IAuthService authService, AppDBContext db)
        {
            _connections = connections;
            _authService = authService;
            _db = db;
        }

        public string nuevoUsuario(Usuario user)
        {
            var usuarioRepetido = _db.Usuarios.FirstOrDefault(x => x.NombreUsuario.Equals(user.NombreUsuario));
            if (usuarioRepetido != null)
                return "-2";

            string passwordHash = BC.HashPassword(user.Contraseña);
            user.Contraseña = passwordHash;

            string pResult = "";
            string pSetencia =
                "INSERT INTO Usuarios " +
                "(Nombre,NombreUsuario,Apellido_Paterno,Apellido_Materno,Contraseña,Rol,Autorizado,EmpleadoID) values (@NOMBRE,@NOMBREUSUARIO,@A_PATERNO,@A_MATERNO,@CONTRASEÑA,@Rol,@Autorizado,@EmpleadoID)";

            try
            {
                _connections.executeBlog(pSetencia, (com) =>
                {

                    com.Parameters.AddWithValue("@NOMBRE", user.Nombre);
                    com.Parameters.AddWithValue("@NOMBREUSUARIO", user.NombreUsuario);
                    com.Parameters.AddWithValue("@A_PATERNO", user.Apellido_Paterno);
                    com.Parameters.AddWithValue("@A_MATERNO", user.Apellido_Materno);
                    com.Parameters.AddWithValue("@CONTRASEÑA", user.Contraseña);
                    com.Parameters.AddWithValue("@Rol", user.Rol);
                    com.Parameters.AddWithValue("@Autorizado", user.Autorizado);
                    com.Parameters.AddWithValue("@EmpleadoID", user.EmpleadoID);
                    Console.WriteLine(user);

                    pResult = com.ExecuteNonQuery().ToString();
                });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                pResult = "-1";
            }

            return pResult;
        }
        public Usuario iniciarSesion(string user, string password)
        {
            var userfound = new Usuario();
            bool isVerified = false;
            string pSentence = @"select * from Usuarios where NombreUsuario=@USER";

            try
            {
                _connections.executeReaderBlogNoAsync(pSentence, new Dictionary<string, object> { { "@USER", user }, { "@CONTRASEÑA", password } }, (reader) =>
                {
                    userfound = new Usuario()
                    {
                        ID = reader["ID"] is DBNull ? Guid.Empty : Guid.Parse(reader["ID"].ToString()),
                        Nombre = reader["Nombre"] is DBNull ? null : reader["Nombre"].ToString(),
                        NombreUsuario = reader["NombreUsuario"] is DBNull ? null : reader["NombreUsuario"].ToString(),
                        Apellido_Materno = reader["Apellido_Materno"] is DBNull ? null : reader["Apellido_Materno"].ToString(),
                        Apellido_Paterno = reader["Apellido_Paterno"] is DBNull ? null : reader["Apellido_Paterno"].ToString(),
                        Fecha_Registro = reader["Fecha_Registro"] is DBNull ? DateTime.Parse("24/02/22") : DateTime.Parse(reader["Fecha_Registro"].ToString()),
                        Contraseña = reader["Contraseña"] is DBNull ? null : reader["Contraseña"].ToString(),
                        Autorizado = reader["Autorizado"] is DBNull ? false : bool.Parse(reader["Autorizado"].ToString()),
                        Rol = reader["Rol"] is DBNull ? null : reader["Rol"].ToString(),
                        EmpleadoID = reader["EmpleadoID"] is DBNull ? Guid.Empty : Guid.Parse(reader["EmpleadoID"].ToString()),


                    };

                    isVerified = BC.Verify(password, userfound.Contraseña);

                    return userfound;
                });

                if (userfound.ID.Equals(Guid.Empty) || !isVerified) throw new Exception("Error de credenciales, Nombre de usuario o contraseña no validos.");
                if (!userfound.Autorizado) throw new Exception("Usuario no autorizado, Comuniquese con el area de sistemas para que le den acceso." );

                return userfound;

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
    }
}
