﻿using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Mantenimiento_tribunaAPI.Persistence
{
    public class Connections
    {
        public Connections(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        public IConfiguration Configuration { get; }
        private SqlConnection GetConnection()
        {
            return new SqlConnection(Configuration.GetConnectionString("ConnectionString"));

        }


        //Insercion a la base de datos
        public void executeBlog(string ASentencie, Action<SqlCommand> ASentencieAction)
        {
            using (SqlConnection con = GetConnection())
            {

                try
                {
                    using (SqlCommand com = new SqlCommand(ASentencie, con))
                    {

                        con.Open();
                        ASentencieAction.Invoke(com);
                    }
                }
                finally
                {
                    if (con.State == System.Data.ConnectionState.Open)
                    {
                        con.Close();
                        con.Dispose();
                    }
                }


            }
        }

        public IEnumerable<T> queryBlogListNoAsync<T>(string ASentencie, Dictionary<string, object> AParams, Func<SqlDataReader, T> f)
        {
            List<T> pResult = new List<T>();

            using (SqlConnection con = GetConnection())
            {

                try
                {
                    using (SqlCommand com = new SqlCommand(ASentencie, con))
                    {
                        foreach (Match match in Regex.Matches(ASentencie, @"(?<!\w)\@\w+"))
                        {
                            string pKey = match.Value.ToUpper();
                            com.Parameters.AddWithValue(pKey, AParams[pKey]);

                        }
                        con.Open();
                        using (SqlDataReader reader = com.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                pResult.Add(f.Invoke(reader));
                            }
                        }

                    }
                }
                finally
                {
                    if (con.State == System.Data.ConnectionState.Open)
                    {
                        con.Close();
                        con.Dispose();
                    }
                }
            }
            return pResult;
        }




        public T executeReaderBlogNoAsync<T>(string ASentencia, Dictionary<string, object> AParams, Func<SqlDataReader, T> AData)
        {
            try
            {
                T pResult = default(T);

                SqlConnection con = GetConnection();
                SqlCommand com = new SqlCommand(ASentencia, con);

                foreach (Match match in Regex.Matches(ASentencia, @"(?<!\w)\@\w+"))
                {
                    string pKey = match.Value.ToUpper();
                    com.Parameters.AddWithValue(pKey, AParams[pKey]);
                }

                con.Open();

                SqlDataReader reader = com.ExecuteReader();

                if (reader.Read())
                    pResult = AData(reader);

                if (con.State == System.Data.ConnectionState.Open)
                    con.Close();

                return pResult;
            }
            catch
            {
                return default(T);
            }
        }
    }

}
