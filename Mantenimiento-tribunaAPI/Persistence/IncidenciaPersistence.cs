﻿using Mantenimiento_tribunaAPI.Context;
using Mantenimiento_tribunaAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mantenimiento_tribunaAPI.Persistence
{
    public class IncidenciaPersistence
    {
        private Connections _connections;
        private AppDBContext _appContext;

        public IncidenciaPersistence(Connections connections, AppDBContext appDBContext)
        {
            _connections = connections;
            _appContext = appDBContext;
        }

        public string nuevaIncidencia(Bitacora bit)
        {
            string pResult = "";
            string pSetencia =
                "INSERT INTO Incidencias " +
                "(Tipo,Area,Plaza,UsuarioID,Prioridad,Problema,Estado) values (@TIPO,@AREA,@PLAZA,@USUARIOID,@PRIORIDAD,@PROBLEMA,@Estado)";

            try
            {
                _connections.executeBlog(pSetencia, (com) =>
                {

                    com.Parameters.AddWithValue("@TIPO", bit.Tipo);
                    com.Parameters.AddWithValue("@AREA", bit.Area);
                    com.Parameters.AddWithValue("@PLAZA", bit.Plaza);
                    com.Parameters.AddWithValue("@USUARIOID", bit.UsuarioID);
                    com.Parameters.AddWithValue("@PRIORIDAD", bit.Prioridad);
                    com.Parameters.AddWithValue("@PROBLEMA", bit.Problema);
                    com.Parameters.AddWithValue("@Estado", bit.Estado);
                    Console.WriteLine(bit);

                    pResult = com.ExecuteNonQuery().ToString();
                });
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex);
                pResult = "-1";
            }

            return pResult;
        }
        public Bitacora resolverIncidencia(RespuestaIncidencia bit)
        {
            string pSentence = @$"select * from Incidencias where ID = '{bit.IdRef}'";


            try
            {
                return _connections.executeReaderBlogNoAsync(pSentence, new Dictionary<string, object> { { "", "" } }, reader =>
                {

                    return new Bitacora()
                    {
                        ID = reader["ID"] is DBNull ? Guid.Empty : Guid.Parse(reader["ID"].ToString()),
                        Estado = reader["Estado"] is DBNull ? "Abierto" : reader["Estado"].ToString(),
                        Prioridad = reader["Prioridad"] is DBNull ? null : reader["Prioridad"].ToString(),
                        Problema = reader["Problema"] is DBNull ? null : reader["Problema"].ToString(),
                        Plaza = reader["Plaza"] is DBNull ? null : reader["Plaza"].ToString(),
                        Responsable = reader["Responsable"] is DBNull ? null : reader["Responsable"].ToString(),
                        Solucion = reader["Solucion"] is DBNull ? null : reader["Solucion"].ToString(),
                        Area = reader["Area"] is DBNull ? null : reader["Area"].ToString(),
                        Categoria = reader["Categoria"] is DBNull ? null : reader["Categoria"].ToString(),
                        Tiempo_Invertido = reader["Tiempo_Invertido"] is DBNull ? 0 : double.Parse(reader["Tiempo_Invertido"].ToString()),
                        Fecha_Registro = reader["Fecha_Registro"] is DBNull ? default(DateTime) : DateTime.Parse(reader["Fecha_Registro"].ToString()),
                        Fecha_Cierre = reader["Fecha_Cierre"] is DBNull ? default(DateTime) : DateTime.Parse(reader["Fecha_Cierre"].ToString()),
                        Tipo = reader["Tipo"] is DBNull ? null : reader["Tipo"].ToString(),
                        UsuarioID = reader["UsuarioID"] is DBNull ? Guid.Empty : Guid.Parse(reader["UsuarioID"].ToString())

                    };
                });
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public IEnumerable<Bitacora> obtenerIncidencias()
        {
            string pSentence = @$"select * from Incidencias where Estado = 'Abierto' ";


            try
            {
                return _connections.queryBlogListNoAsync(pSentence, new Dictionary<string, object> { { "", "" } }, reader =>
                {

                    return new Bitacora()
                    {
                        ID = reader["ID"] is DBNull ? Guid.Empty : Guid.Parse(reader["ID"].ToString()),
                        Estado = reader["Estado"] is DBNull ? "Abierto" : reader["Estado"].ToString(),
                        Prioridad = reader["Prioridad"] is DBNull ? null : reader["Prioridad"].ToString(),
                        Problema = reader["Problema"] is DBNull ? null : reader["Problema"].ToString(),
                        Plaza = reader["Plaza"] is DBNull ? null : reader["Plaza"].ToString(),
                        Responsable = reader["Responsable"] is DBNull ? null : reader["Responsable"].ToString(),
                        Solucion = reader["Solucion"] is DBNull ? null : reader["Solucion"].ToString(),
                        Area = reader["Area"] is DBNull ? null : reader["Area"].ToString(),
                        Categoria = reader["Categoria"] is DBNull ? null : reader["Categoria"].ToString(),
                        Tiempo_Invertido = reader["Tiempo_Invertido"] is DBNull ? 0 : double.Parse(reader["Tiempo_Invertido"].ToString()),
                        Fecha_Registro = reader["Fecha_Registro"] is DBNull ? default(DateTime) : DateTime.Parse(reader["Fecha_Registro"].ToString()),
                        Fecha_Cierre= reader["Fecha_Cierre"] is DBNull ? default(DateTime) : DateTime.Parse(reader["Fecha_Cierre"].ToString()),
                        Tipo = reader["Tipo"] is DBNull ? null : reader["Tipo"].ToString(),
                        UsuarioID = reader["UsuarioID"] is DBNull ? Guid.Empty : Guid.Parse(reader["UsuarioID"].ToString())

                    };
                });
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
        public IEnumerable<Bitacora> incidencias_Resueltas()
        {
            string pSentence = @$"select * from Incidencias where Estado = 'Cerrado' ";


            try
            {
                return _connections.queryBlogListNoAsync(pSentence, new Dictionary<string, object> { { "", "" } }, reader =>
                {

                    return new Bitacora()
                    {
                        ID = reader["ID"] is DBNull ? Guid.Empty : Guid.Parse(reader["ID"].ToString()),
                        Estado = reader["Estado"] is DBNull ? "Abierto" : reader["Estado"].ToString(),
                        Prioridad = reader["Prioridad"] is DBNull ? null : reader["Prioridad"].ToString(),
                        Problema = reader["Problema"] is DBNull ? null : reader["Problema"].ToString(),
                        Plaza = reader["Plaza"] is DBNull ? null : reader["Plaza"].ToString(),
                        Responsable = reader["Responsable"] is DBNull ? null : reader["Responsable"].ToString(),
                        Solucion = reader["Solucion"] is DBNull ? null : reader["Solucion"].ToString(),
                        Area = reader["Area"] is DBNull ? null : reader["Area"].ToString(),
                        Categoria = reader["Categoria"] is DBNull ? null : reader["Categoria"].ToString(),
                        Tiempo_Invertido = reader["Tiempo_Invertido"] is DBNull ? 0 : double.Parse(reader["Tiempo_Invertido"].ToString()),
                        Fecha_Registro = reader["Fecha_Registro"] is DBNull ? default(DateTime) : DateTime.Parse(reader["Fecha_Registro"].ToString()),
                        Fecha_Cierre = reader["Fecha_Cierre"] is DBNull ? default(DateTime) : DateTime.Parse(reader["Fecha_Cierre"].ToString()),
                        Tipo = reader["Tipo"] is DBNull ? null : reader["Tipo"].ToString(),
                        UsuarioID = reader["UsuarioID"] is DBNull ? Guid.Empty : Guid.Parse(reader["UsuarioID"].ToString())

                    };
                });
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}
