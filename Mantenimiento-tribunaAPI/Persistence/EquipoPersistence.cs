﻿using Mantenimiento_tribunaAPI.Context;
using Mantenimiento_tribunaAPI.Models;
using Mantenimiento_tribunaAPI.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Mantenimiento_tribunaAPI.Persistence
{
    public class EquipoPersistence
    {
        private AppDBContext _appBDContext;
        public EquipoPersistence(AppDBContext appBDContext)
        {
            _appBDContext = appBDContext;
        }
        public Equipo_Inventario asignarEquipo(Asignacion model)
        {
            var res = _appBDContext.Equipos.FirstOrDefault(x => x.ID.Equals(model.equipoID));
            if (res != null)
            {
                res.Estado = "Asignado";
                res.Usuario_Actual = model.emploeadoID;
                _appBDContext.SaveChanges();
                return res;
            }

            return res;
        }
        public List<Equipo_Inventario> misAsignados(Guid empleadoID)
        {
            return _appBDContext.Equipos.Where(x => x.Usuario_Actual.Equals(empleadoID)).ToList();
        }
        public string devolverEquipo(Guid equipoID)
        {
            var res = _appBDContext.Equipos.FirstOrDefault(x => x.ID.Equals(equipoID));
            if (res != null)
            {
                res.Estado = "Libre";
                res.Usuario_Actual = null;
                _appBDContext.SaveChanges();
                return "Devolucion terminada";
            }
            return null;
        }
        public Equipo_Inventario actualizarEquipo(Equipo_Inventario model, Guid ID)
        {
            var equipoEncontrado = _appBDContext.Equipos.FirstOrDefault(x => x.ID.Equals(ID));
            if (equipoEncontrado != null)
            {
                try
                {
                    equipoEncontrado.Marca = model.Marca;
                    equipoEncontrado.Modelo = model.Modelo;
                    equipoEncontrado.Nombre_Equipo = model.Nombre_Equipo;
                    equipoEncontrado.N_Serie = model.N_Serie;
                    equipoEncontrado.N_Telefono = model.N_Telefono;
                    equipoEncontrado.Office = model.Office;
                    equipoEncontrado.Plan = model.Plan;
                    equipoEncontrado.Procesador = model.Procesador;
                    equipoEncontrado.Razon_Social = model.Razon_Social;
                    equipoEncontrado.Region = model.Region;
                    equipoEncontrado.SO = model.SO;
                    equipoEncontrado.Tipo = model.Tipo;
                    equipoEncontrado.Usuario_Actual = model.Usuario_Actual;
                    equipoEncontrado.Cuenta = model.Cuenta;
                    equipoEncontrado.Cuenta_Padre = model.Cuenta_Padre;
                    equipoEncontrado.Disco_Duro = model.Disco_Duro;
                    equipoEncontrado.Estado = model.Estado;
                    equipoEncontrado.Fecha_Registro = DateTime.Now;
                    _appBDContext.SaveChanges();
                }
                catch (Exception)
                {
                    throw;
                }
            }
            return equipoEncontrado;
        }
        public Equipo_Inventario equipoInfo(Guid ID)
        {
            return _appBDContext.Equipos.FirstOrDefault(x => x.ID.Equals(ID));
        }
        public List<Equipo_Inventario> obtenerEquipos()
        {
            return _appBDContext.Equipos.ToList();
        }
        internal Equipo_Inventario registrarEquipo(Equipo_Inventario model)
        {
            try
            {
                model.Fecha_Registro = DateTime.Now;
                var res = _appBDContext.Equipos.Add(model);
                _appBDContext.SaveChanges();
            }
            catch (Exception)
            {
                throw new Exception("Ocurrio un error al registrar");
            }
            return model;
        }
        public List<Equipo_Inventario> equiposLibres(string tipo)
        {
            List<Equipo_Inventario> lista = new List<Equipo_Inventario>();
            if (tipo == "mov")
            {
                lista = _appBDContext.Equipos.Where(x => (x.Tipo == "Smartphone" || x.Tipo == "Tablet") && x.Estado == "Libre").ToList();
                if (lista.Count > 0 || lista != null)
                    return lista;
            }

            if (tipo == "comp")
            {
                lista = _appBDContext.Equipos.Where(x => (x.Tipo == "Desktop" || x.Tipo == "Laptop") && x.Estado == "Libre").ToList();
                if (lista.Count > 0 || lista != null)
                    return lista;
            }

            if (tipo == "chip")
            {
                lista = _appBDContext.Equipos.Where(x => (x.Tipo == "Chip") && x.Estado == "Libre").ToList();
                if (lista.Count > 0 || lista != null)
                    return lista;
            }
            var otros = _appBDContext.Equipos.Where(x => (x.Tipo != "Smartphone" && x.Tipo != "Tablet" && x.Tipo != "Desktop" && x.Tipo != "Laptop"
            && x.Tipo != "Chip") && x.Estado == "Libre").ToList();
            if (otros.Count > 0 || otros != null)
                return otros;

            return lista;
        }
    }
}

